# Partie 1 : Commandes avancées Git

1. fork le projet en créant un nouveau sur mon repo  

2. Executer les tests : 
    - pip install pytest
    - python pytest -v
La fonction et le test de fibonacci comporte une erreur :
changement des erreur suivantes:
    - loop_count -> next
    - testFibonacci -> test_fibonacci

3. Git branch bug-fix/fibonacci

6. Git bisect start <bad_commit_id> <good_commit_id> 
    - ensuite pour chaque commit je lance les test et si problème il y a je lance la commande `git bisect bad`, le cas contraire `git bisect good`
    - une fois tous les commit traités je quitte le bisect `git bisect reset`

7. Git blame [fileName] [ligneStart] [ligneEnd] 
on peut ainsi voir les resposables du commit ligne par ligne

8. Se mettre dans production et cherry-pick le commit sélectionné par le git bisect
   Git cherry-pick <correction_commit_id>

9. Git merge bug-fix/fibonacci

10. Git format-patch -o <correction_commit_id>

# Partie 2 : Collaboration de code avec gitlab

Voir le README sur le depot de Julien Deramaix : https://gitlab.univ-lr.fr/jderamai/math-python/-/blob/main/README.md

# Partie 3 : Utilisation de pre-commit pour python

1. J'ai créer un fichier requirements-dev.txt et j'ai mis pre-commit. Ensuite, j'ai installé pre-commit : `pip install -r requirements-dev.txt`

2. J'ai configuré un pre-commit en créeant le fichier
   .pre-commit-config.yaml

3. repos:
      - repo: https://github.com/pre-commit/mirrors-isort
         rev: v5.5.4
         hooks:
            - id: isort
            additional_dependencies: [black]
            args: ["--profile", "black"]

      - repo: https://github.com/psf/black
         rev: stable
         hooks:
            - id: black

4. pre-commit run --all-files
